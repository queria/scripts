#!/bin/bash

MSG="$*"
[[ -z "$MSG" ]] && read -t 10 -p "Type custom message, 'names' or just press enter" MSG
[[ -z "$MSG" ]] && MSG="test message"

if [[ "$MSG" = "names" ]]; then
    declare -a CLRS
    CLRS=("BlackGrey" "Red" "Green" "YellowOrange" "Blue" "PurpleMagenta" "Cyan" "White")
    #CLRS=( [0]=BlackGray [1]=Red )
    #CLRS=( [BlackGray]=0 [Red]=1 [Green]=2 [Orange]=3 [Blue]=4 [Magenta]=5 [Cyan]=6 [White]=7 )
    RAINBOW=""
    for CLR in "${!CLRS[@]}"; do
        C="\033[0;3${CLR}m";
        echo -e "${C}\\\033[0;3${CLR}m ${CLRS[$CLR]}\033[00m";
        RAINBOW="${RAINBOW}${C}abcABC\033[00m"
        C="\033[0;9${CLR}m";
        echo -e "${C}\\\033[0;9${CLR}m Intense ${CLRS[$CLR]}\033[00m";
        C="\033[1;3${CLR}m";
        echo -e "${C}\\\033[1;3${CLR}m Bold ${CLRS[$CLR]}\033[00m";
    done
    echo ""
    echo -e "${RAINBOW}"

else

    for CLR in $(seq 36); do
        for CTP in 0 1; do
            C="\033[0${CTP};${CLR}m";
            echo -e "${C}\\\033[0${CTP};${CLR}m ${MSG}\033[00m";
        done
    done

fi

echo ""
echo -e "\033[0m\\\033[0m  to reset"
