#!/bin/bash
set -ex

if [[ -z "$1" || ! -x "./${1}" ]]; then
	echo Specify parameter with executable file.
	echo Like 'NVIDIA-Linux-.*.run'
	exit 1
fi
DRV_FILE="$1"
shift

systemctl stop prometheus-nvidia-exporter.service
systemctl stop sddm
sleep 0.3

if lsmod | grep -q nvidia; then
	rmmod nvidia_drm
	rmmod nvidia_modeset
	rmmod nvidia
fi

"./$DRV_FILE" --ui=none --no-questions --install-libglvnd -j 8 --dkms "$@"

sleep 0.3
systemctl start prometheus-nvidia-exporter.service
#systemctl start sddm
