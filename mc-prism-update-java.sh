#!/bin/bash
set -eo pipefail
function redMsg { echo -e "\033[1;31m$@\033[00m"; }
function grayMsg { echo -e "\033[1;30m$@\033[00m"; }

PRETEND=yes
[[ "$1" != "--do" ]] || PRETEND=no

if [[ "yes" == "$PRETEND" ]]; then
    grayMsg "// Just pretending, to actually perform any changes pass --do as first option."
fi
[[ "$1" != "--help" ]] || exit

if [[ "$1" == "--update" ]]; then
    set -x
    curl -L "https://gitlab.com/queria/scripts/-/raw/master/mc-prism-update-java.sh" -o "$0"
    chmod +x "$0"
    exit
fi

LAUNCHER="$(dirname "$(readlink -f "$0")")"
mkdir -p $LAUNCHER/java
cd $LAUNCHER/java


function get_java {
    local javaDir="$1"
    local sourceUrl="$2"
    local downloadType="$3"

    local archiveFile=$(basename "$sourceUrl")

    if [[ ! -f "$javaDir/bin/java" ]]; then
        if [[ ! -f "$archiveFile" ]]; then
            if [[ "manual" == "$downloadType" ]]; then
                redMsg "Download $javaDir manually from $sourceUrl"
                return 1
            else
                if [[ "yes" == "$PRETEND" ]]; then
                    echo "* would fetch $sourceUrl" >&2
                    return
                fi
                (set -x; curl -L -O "$sourceUrl")
            fi
        fi

        local origDir=$(tar -tf "$archiveFile" | head -n1)

        if [[ -z "$origDir" ]]; then
            echo "ERROR: Unable to detect original dir name for $archiveFile"
            return 1
        fi

        if [[ ! -d "$origDir" ]]; then
            if [[ "yes" == "$PRETEND" ]]; then
                echo "* would extract $sourceUrl" >&2
                return
            fi
            (set -x; tar xf "$archiveFile")
        fi
        if [[ "$origDir" != "$javaDir" ]]; then
            # auto-rename folders
            if [[ "yes" == "$PRETEND" ]]; then
                echo "* would rename $origDir to $javaDir" >&2
                return
            fi
            (set -x; mv "$origDir" "$javaDir")
        fi
    fi
}

# get links from either:
# - https://adoptium.net/temurin/archive/?version=18 (go to source repo release page)
# - https://www.oracle.com/java/technologies/oracle-java-archive-downloads.html (requires manual download, so avoid as much as possible)

# no 1.7 on adoptium/... so stick with oracle one
get_java "jre-1.7" "https://download.oracle.com/otn/java/jdk/7u80-b15/jre-7u80-linux-x64.tar.gz" "manual"
# newer builds of 1.8 above u312 have issue with some mods (obfuscate/mccrayfish guns)
get_java "java-1.8-openjdk" "https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u312-b07/OpenJDK8U-jre_x64_linux_hotspot_8u312b07.tar.gz"
#
get_java "java-11-openjdk" "https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.17%2B8/OpenJDK11U-jre_x64_linux_hotspot_11.0.17_8.tar.gz"
get_java "java-16-openjdk" "https://github.com/adoptium/temurin16-binaries/releases/download/jdk-16.0.2%2B7/OpenJDK16U-jdk_x64_linux_hotspot_16.0.2_7.tar.gz"
get_java "java-17-openjdk" "https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.5%2B8/OpenJDK17U-jre_x64_linux_hotspot_17.0.5_8.tar.gz"
get_java "java-18-openjdk" "https://github.com/adoptium/temurin18-binaries/releases/download/jdk-18.0.2.1%2B1/OpenJDK18U-jre_x64_linux_hotspot_18.0.2.1_1.tar.gz"
get_java "java-19-openjdk" "https://github.com/adoptium/temurin19-binaries/releases/download/jdk-19.0.1%2B10/OpenJDK19U-jre_x64_linux_hotspot_19.0.1_10.tar.gz"
get_java "java-20-openjdk" "https://github.com/adoptium/temurin20-binaries/releases/download/jdk-20.0.2%2B9/OpenJDK20U-jre_x64_linux_hotspot_20.0.2_9.tar.gz"
get_java "java-21-openjdk" "https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.4%2B7/OpenJDK21U-jre_x64_alpine-linux_hotspot_21.0.4_7.tar.gz"

while read J; do
    JS="$J"
    # for system openjdk installations
    JS="${JS#/usr/lib/jvm/}";
    JS="${JS%-openjdk*/bin/java}";
    JS="${JS%-temurin*/bin/java}";
    # for local versions
    JS="${JS#./}"
    JS="$(sed -r "s%^j(dk|re)-?([0-9u]+(\.[0-9u]+)?).*/bin/java%java-\2%" <<<"$JS")"

    J=$(readlink -f "$J")
    if [[ "$1" != "--show" ]]; then
        echo "$J => $HOME/bin/$JS" >&2
    fi
    if [[ "no" == "$PRETEND" ]]; then
        ln -snf "$J" "$HOME/bin/$JS";
    fi
done < <(set +e; ls -1 /usr/lib/jvm/java-*-{openjdk,temurin}/bin/java 2>/dev/null; ls -1 ./*/bin/java)

cd $LAUNCHER
IFSorig="$IFS"
IFS=$'\n'
badConfigs=()
for CFG in $(echo prismlauncher.cfg; find instances/ -maxdepth 2 -type f -name instance.cfg); do
    javaPath=$(sed -nr 's/^JavaPath=//p' "$CFG")
    if [[ -n "$javaPath" ]]; then
        if ! command -v "$javaPath" &> /dev/null; then
            badConfigs+=("$CFG")
            redMsg "$javaPath used in $CFG is MISSING"
        elif [[ "$1" == "--show" ]]; then
            echo "$javaPath used in $CFG"
        fi
    fi
done
IFS=$IFSorig
if [[ "$1" == "--edit" ]]; then
    vim -p "${badConfigs[@]}"
fi
