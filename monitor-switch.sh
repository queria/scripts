#!/bin/bash

if [[ "$1" = "-v" ]]; then
    set -x
    shift
fi

SAFEONLY=true
if [[ "$1" = "--safe" ]]; then
    SAFEONLY=true
    shift
fi

###### TEMPORARY SECTION FOR KDE-WAYLAND SUPPORT
if [[ "KDE" == "${XDG_SESSION_DESKTOP}" && -n "$(command -v kscreen-doctor)" ]]; then
    # is there better way for detecting default/builtin display?
    DEFAULT=$(kscreen-doctor -j | jq -r '.outputs[] | select(.id == 1) | .name')
    SECOND=$(kscreen-doctor -j | jq -r '.outputs | map(select(.id != 1)).[0] | select(.).name')
    getPrefMode() {
        kscreen-doctor -j | jq -r '.outputs[] | select(.name == "'$1'").preferredModes[0]'
    }
    getModeWidth() {
        local disp=$1
        local mode=$2
        [[ -n "${mode}" ]] || mode=$(getPrefMode "${disp}")
        kscreen-doctor -j | jq -r '.outputs[] | select(.name == "'${disp}'").modes[] | select(.id == "'${mode}'").size.width'
    }
    if [[ -n "$SECOND" && ( "$1" == "split" || "$1" == "split-left" ) ]]; then
        kscreen-doctor \
            output.${SECOND}.mode.$(getPrefMode "${SECOND}") \
            output.${SECOND}.position.0,0 \
            output.${SECOND}.enable \
            output.${DEFAULT}.mode.$(getPrefMode "${DEFAULT}") \
            output.${DEFAULT}.position.$(getModeWidth "${SECOND}"),0 \
            output.${DEFAULT}.enable

    elif [[ -n "${SECOND}" && "$1" == "split-right" ]]; then
        kscreen-doctor \
            output.${DEFAULT}.mode.$(getPrefMode "${DEFAULT}") \
            output.${DEFAULT}.position.0,0 \
            output.${DEFAULT}.enable \
            output.${SECOND}.mode.$(getPrefMode "${SECOND}") \
            output.${SECOND}.position.$(getModeWidth "${DEFAULT}"),0 \
            output.${SECOND}.enable
    elif [[ -n "${SECOND}" && "$1" == "ext" ]]; then
        kscreen-doctor \
            output.${SECOND}.mode.$(getPrefMode "${SECOND}") \
            output.${SECOND}.position.0,0 \
            output.${SECOND}.enable
        kscreen-doctor output.${DEFAULT}.disable
    else # single monitor
        # ensure single is correctly reset
        kscreen-doctor \
            output.${DEFAULT}.mode.$(getPrefMode "${DEFAULT}") \
            output.${DEFAULT}.position.0,0 \
            output.${DEFAULT}.enable
        # disable all additional displays
        if [[ -n "${SECOND}" ]]; then
            disable_rest=$(kscreen-doctor -j | jq -r '.outputs | map(select(.name != "'${DEFAULT}'")) | map("output." + .name + ".disable") | join(" ")')
            [[ -z "${disable_rest}" ]] || kscreen-doctor ${disable_rest}
        fi
        # as there seems to be some bug with KDE main panel disappearing
        # (no idea when exactly, something to do with docking to usb-c display and maybe while screen is locked?)
        # try to force switch the resolution to different and back to default
        kscreen-doctor output.${DEFAULT}.mode.$(( 1 + $(getPrefMode "${DEFAULT}") ))
        kscreen-doctor output.${DEFAULT}.mode.$(getPrefMode "${DEFAULT}")
    fi
    exit
fi
################################################

if ! which xrandr &> /dev/null; then
    echo "No xrandr command available ... you have to fix it."
    exit 1
fi

XROUT=$(xrandr -q)

# use configuration given as argument
CONFIG=$1
# if specified, otherwise try autodetection
if [[ -z "$CONFIG" ]]; then
    if [[ "$(grep " connected " <<<"$XROUT" | wc -l)" = "1" ]]; then
        CONFIG=default
    elif grep -q "DP-1-2 connected" <<<"$XROUT"; then
        CONFIG=work
    elif grep -q "DVI-I-[01] connected" <<<"$XROUT" && grep -q "DVI-D-0 connected" <<<"$XROUT"; then
        CONFIG=home
    elif grep -q "VGA1 connected" <<<"$XROUT"; then
        EXTRA_ID=$(md5sum /sys/class/drm/card0-VGA-1/edid | cut -f1 -d' ')
        echo "EDID: $EXTRA_ID"
        [[ "$EXTRA_ID" = "b3bd2d4d69de88ff12cba4b2d883ec3c" ]] && CONFIG=home
    fi
    # place your autodetection here

    #echo "Autodetected monitor layout: $CONFIG"
fi

#[[ "$CONFIG" = "home2" ]] && DEFAULT=DVI-D-0

#DEFAULT="LVDS-0"
DEFAULT="${DEFAULT:-$(xrandr |sed -rn 's/(.*) connected .*/\1/p'|head -n1)}"

# first everything off to clean possible manual changes,

# also workarounds issue with razor-qt WM, which may calculate positions
# incorrectly after display is switched
# ~ https://github.com/Razor-qt/razor-qt/issues/612

whichstate="(dis)?connected"
$SAFEONLY && whichstate="disconnected"
for DISP in $(sed -nr "s/^(.*) $whichstate.*/\1/p" <<<"$XROUT"); do
    [[ "$DISP" = "$DEFAULT" && $SAFEONLY ]] && continue
    xrandr --output $DISP --off
done

# always switch back to default
# [[ "$CONFIG" != "work" ]] && xrandr --output $DEFAULT --auto --rotate normal --reflect normal --primary

echo $CONFIG > /tmp/mon.log
xrandr >> /tmp/mon.log

case $CONFIG in
    home)
        xrandr --output DVI-D-0 --auto --left-of $DEFAULT --output $DEFAULT --auto --primary
        ;;
    home2)
        xrandr --output DVI-D-0 --off --output $DEFAULT --auto --primary
        ;;
    rust)
        xrandr --output DVI-I-1 --auto --primary --scale-from 1280x720
        ;;
    work)
        xrandr --output DP-1-2 --auto --primary;
        xrandr --output eDP-1 --off;
        xrandr --output DP-1-2 --auto --primary;
        #xrandr --output DP-2-2 --auto --primary
        ;;
    present)
        xrandr --output VGA1 --auto --left-of $DEFAULT --output $DEFAULT --auto --primary
        ;;
    present-mirror)
        xrandr --output VGA1 --same-as $DEFAULT
    #    xrandr --output VGA1 --off --output $DEFAULT --auto --rotate normal --reflect normal --primary
        ;;
    small)
        xrandr --output $DEFAULT --scale 0.67x0.67
        ;;
    default)
        xrandr --output $DEFAULT --primary --auto
        # use just display of nb
        #xrandr --output HDMI3 --off --output $DEFAULT --auto --rotate normal --reflect normal --primary
        # do nothing as we should be in the default now
        ;;
    -h|--help|help|*)
        echo "Usage:"
        echo "$0 [work | default]" # list possible configurations here
        ;;
esac

