#!/bin/bash
if [[ "$1" == "--help" ]]; then
    echo "$0 [--debug] <url-to-web-directory-index>"
    echo ""
    echo "This will fetch directory index page (like from apache httpd)"
    echo "and convert it to m3u file (just list of video/audio urls)."
    exit 0
fi

TMP_HTML=$(mktemp)
cleanup() {
    if [[ ! -z "$TMP_HTML" && -f "$TMP_HTML" ]]; then
        rm -f "$TMP_HTML"
    fi
}
trap cleanup EXIT

wget_verbosity="--quiet"
if [[ "$1" == "--debug" ]]; then
    shift
    wget_verbosity="--verbose"
    set -x
fi

wget $wget_verbosity --convert-links "$1" -O "$TMP_HTML"
# using var here so content shows when in debug mode
HTML=$(cat "$TMP_HTML")
sed -nr 's/.*"(http.*(mkv|avi|mp4|ogg|mp3|mov))".*/\1/Ip' <<<"$HTML"
