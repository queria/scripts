#!/bin/bash

# run this with all mentioned terminals installed,
# and from a directory where all 'st-*' compiled binaries are
# (in my case multiple builds of 'st' with different patches or optimizations used)

# modify/comment-out any terminals you do(not) want to test
# as possibly any commands/sections at the end

test_cmd() {
    # run tested command few times and print out average time it took finish
    local T="0";
    local RETRIES=${RETRIES:-5}
    for i in $(seq $RETRIES); do
        T="$T + $(/usr/bin/time "$@" |& sed -nr 's/.*:([0-9.]+)elapsed.*/\1/p')"
    done;
    echo "$(bc <<<"($T) / $RETRIES") $@"
}

setup() {
    # prepare resources needed for execution of test-cases
    if [[ ! -f "/tmp/long.txt" ]]; then
        curl --silent "https://raw.githubusercontent.com/GNOME/vte/master/doc/boxes.txt" -o /tmp/long.txt
        for x in $(seq 10); do
            cat /tmp/long.txt >> /tmp/long2.txt
            cat /tmp/long.txt >> /tmp/long2.txt
            mv /tmp/long2.txt /tmp/long.txt
        done
    fi

    if [[ ! -f "/tmp/usr_list.txt" ]]; then
        ls -Ral /usr/share /usr/lib /usr/src /usr/include > /tmp/usr_list.txt
    fi
}

with_all_terminals() {
    # list of supported terminals - and their correct accepted parameter formats
    # each terminal gets run with command specified in params

    CMD="$@"
    #test_cmd gnome-terminal --wait -- $CMD
    #test_cmd xfce4-terminal -e "$CMD"

    #test_cmd terminology -e $CMD
    #test_cmd alacritty -e $CMD
    #test_cmd kitty -e $CMD
    test_cmd urxvt -e $CMD
    #test_cmd urxvt256c -e $CMD
    #for ST in st-*; do
    #for ST in st-l200-o3native; do
    #    test_cmd ./$ST -e $CMD
    #done
}

echo "== setup =="
setup
echo "== pre-heat =="
RETRIES=1 with_all_terminals /usr/bin/true > /dev/null


### Test cases

echo "== test: startup =="
with_all_terminals /usr/bin/true | sort -h
echo ""

echo "== test: long utf8 text (boxes) =="
with_all_terminals cat /tmp/long.txt | sort -h
echo ""

echo "== test: long text (/usr) =="
with_all_terminals cat /tmp/usr_list.txt | sort -h
echo ""

echo "== test: interactive shell start =="
with_all_terminals $SHELL -i -c /usr/bin/true | sort -h
echo ""

