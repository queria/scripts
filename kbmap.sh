#!/bin/bash

override=""
if [[ "$1" != "help" && "$1" != "-h" && "$1" != "--help" ]]; then
    override="$1"
fi

current=$(setxkbmap -query|sed -rn 's/layout: +(.*)/\1/p')

next=vok
[[ cz != $current ]] && next=cz
next=${override:-$next}
if [[ $next = vok ]]; then
    # use 'user-config' setup of vok
    setxkbmap en_US
    setxkbmap vok -print | xkbcomp -I$HOME/.xkb - $DISPLAY 2>/dev/null
else
    setxkbmap $next
fi
echo "$(date): configuring $next" >> /tmp/qskbmap-${USER}.log
if command -v osd_cat &> /dev/null; then
    echo "layout: $next" | osd_cat -d1 -o100 -f "-*-fixed-*-*-*-*-*-140-*-*-*-*-*-*"
else
    echo "Switched to $next kb layout"
fi
