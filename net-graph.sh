#!/bin/bash

STAMP="$(date "+%y%m%d_%H_%M_%S")"
LDIR="/var/log/net-graph"
RRFILE="${LDIR}/net.rrd"

# TWEAK THOSE TO YOUR USE CASE
GWIP=192.168.1.254
PUBIP=8.8.8.8
# how often (in minutes) is this executed
# set accordingly to your cron job config
FREQMIN=5
##############################

# atm this keeps records for about 24 days
# and graph is generated for last 7 days
# not configurable atm

[[ -d "$LDIR" ]] || mkdir -p "$LDIR"
cd "$LDIR"
find -type f -name \*.log -mtime +24 -delete

if [[ ! -f "$RRFILE" ]]; then
    FREQ=$(( $FREQMIN * 60 ))
    HB=$(( $FREQ + 120 ))

    rrdtool create $RRFILE \
        --step $FREQ \
        DS:ping-gw:GAUGE:$HB:0:10 \
        DS:ping-pub:GAUGE:$HB:0:300 \
        RRA:LAST:0:1:4040
fi

OUT1=$(ping -n -D -O -c1 192.168.100.254 2>&1)
OUT2=$(ping -n -D -O -c1 8.8.8.8 2>&1)

pinggw=$(sed -nr 's/.*time=(.*) ms/\1/p' <<<"$OUT1")
pingpub=$(sed -nr 's/.*time=(.*) ms/\1/p' <<<"$OUT2")

cat <<< "$OUT1" > "${LDIR}/${STAMP}.log"
cat <<< "$OUT2" >> "${LDIR}/${STAMP}.log"
echo "pinggw=${pinggw:-U}" >> "${LDIR}/${STAMP}.log"
echo "pingpub=${pingpub:-U}" >> "${LDIR}/${STAMP}.log"

rrdtool update $RRFILE N:${pinggw:-U}:${pingpub:-U}

rrdtool graph net.png \
    -w '1280' -h '768' \
    -s 'now-7day' \
    -e 'now-1sec' \
    --x-grid HOUR:12:DAY:1:DAY:1:86400:%a\ %d.%m. \
    --color 'GRID#CECEFE' \
    --color 'MGRID#8484CE' \
    -t 'ping' \
    'DEF:gw=net.rrd:ping-gw:LAST' \
    'CDEF:gw_clean=gw,UN,-1,gw,IF' \
    'DEF:pub=net.rrd:ping-pub:LAST' \
    'CDEF:pub_clean=pub,UN,-1,pub,IF' \
    'LINE1:gw_clean#0000FF' \
    'LINE2:pub_clean#FF0000' > /dev/null

    #'CDEF:user_clean=user_avg,UN,0,user_avg,IF' \
    #'DEF:system_avg=cpu-system.rrd:value:AVERAGE' \
    #'CDEF:system_clean=system_avg,UN,0,system_avg,IF' \
    #'CDEF:user_stack=system_clean,user_clean,+' \
    #'AREA:user_clean#FFF000:user' \
    #'AREA:system_clean#FF0000:system' \
    #'LINE1:user_clean#FF0000'

exit 0
