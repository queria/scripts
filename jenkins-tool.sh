#!/bin/bash
# vim: set et ts=4 sw=4:

### == jenkins-tool.sh [-j <jenkins-id>] <action> ==
###
### Example usage:
###
###  jenkins-tool.sh [-j stage] exec <path/to/script.groovy>
###   ~ execute custom groovy script (on specified [stage] jenkins)
###
###  jenkins-tool.sh enable special-jobs [--do]
###   ~ enables all .*special-jobs.* on local(host) jenkins
###
###  jenkins-tool.sh -j stage queue [pattern] [--do]
###   ~ remove all (without pattern) job runs currently waiting in the queue on 'stage' jenkins
###
###  jenkins-tool.sh -j stage build <jenkins_name> <job_name> [param1=value1 param2=value ...]
###   ~ trigger job build with specified parameters
###
###  jenkins-tool.sh copy <jenkins_name:job_name> <jenkins_name:job_name>
###   ~ fetch job config from source jenkins
###   ~ upload it as new/updated job config into target jenkins
###   ~ both jenkinses to be known in config (see below) esp CURLOPTS with credentials
###   ~ action 'copy-keep' can be used to leave the job config xml behind for inspection/editing
###
###
### Example config file:
### cat ~/.jenkins-tool.conf
###
### CLIOPTS="your-proxy:port"  # for all connections/servers
### JENKINS_local=http://localhost/jenkins
### CLIOPTS_local="-i $HOME/.ssh/id_rsa"
### JENKINS_stage="https://stage.jenkins.company.example.org:88/"
### CLIOPTS_stage="-noCertificateCheck -noKeyAuth"
### CURLOPTS_stage="--user yourUser:apiToken"



set -e

PREVIEW="${PREVIEW:-true}"
SEARCHFOR="${SEARCHFOR:-}"
ACTION="${ACTION:-}"

JENKINS_local="http://localhost/"
JENKINS="${JENKINS:-local}"
CLIOPTS="${CLIOPTS:-}"

CFG="$HOME/.jenkins-tool.conf"

[[ -f "$CFG" ]] && source "$CFG"

while [[ $# -gt 0 ]]; do
    case $1 in
        --help|-h|help)
            ACTION=""
            break
            ;;
        enable)
            ACTION=enable
            ;;
        disable)
            ACTION=disable
            ;;
        queue)
            ACTION=queue
            ;;
        exec)
            ACTION=script
            PREVIEW="false"
            shift
            SCRIPT=$1
            ;;
        build)
            ACTION=build
            shift
            JENKINS=$1
            shift
            JOB_NAME=$1
            shift
            # rest are params
            break
            ;;
        copy|copy-keep)
            [[ "$1" != "copy-keep" ]] || KEEP_XML=true
            ACTION=copy
            ;;
        -j|--jenkins)
            shift
            JENKINS=$1
            ;;
        --do)
            PREVIEW="false"
            ;;
        *)
            if [[ "$ACTION" == "copy" ]]; then
                if [[ -z "$COPY_SRC" ]]; then
                    COPY_SRC="$1"
                    shift
                    continue
                elif [[ -z "$COPY_TGT" ]]; then
                    COPY_TGT="$1"
                    shift
                    continue
                fi
            fi
            SEARCHFOR="$1"
            ;;
    esac
    shift
done

if [[ -z "$ACTION" ]]; then
    # show help
    sed -nr "s/^###(.*)$/\1/p" "$0"
    echo ""
    echo "Configured jenkins connections:"
    echo ""
    if [[ -f "$CFG" ]]; then
        cat $CFG
    else
        echo "No $CFG file yet!"
    fi

    exit 0
fi
PREVIEW_MSG="// JUST PREVIEW, use --do to act. execute the change."
URLVAR="JENKINS_${JENKINS}"
URL="${!URLVAR}"
if [[ -z "${URL}" ]]; then
    echo "$URLVAR does not contain jenkins base url!" >&2
    exit 1
elif [[ "$ACTION" != "copy" ]]; then
    # copy action uses multiple jenkinses, so not applied there
    echo "Using jenkins $URL"
fi
OPTSVAR="CLIOPTS_${JENKINS}"
EXTRAOPTS="${!OPTSVAR}"
EXTRAOPTS="$CLIOPTS $EXTRAOPTS"

CURL=curl

if [[ "$PREVIEW" = "true" ]]; then
    CURL="echo curl"
    echo "$PREVIEW_MSG"
    echo ""
elif [[ -z "$SEARCHFOR" && "$ACTION" =~ (enable|disable|queue) ]]; then
    read -p "Empty pattern, really run on all items? [yN]: " ANSWER
    [[ "$ANSWER" != "y" ]] && exit 0
fi



if [[ "$ACTION" = "build" ]]; then
    echo build-test
    TGT_CURLOPTS="CURLOPTS_${JENKINS}"
    TGT_CURLOPTS="${CURLOPTS:-} ${!TGT_CURLOPTS}"
    PARAMS=()
    while [[ $# -gt 0 ]]; do
      PARAMS=(${PARAMS[@]} "--data-urlencode" "${1}")
      shift
    done
    $CURL --fail -X POST -L $TGT_CURLOPTS "${URL}job/${JOB_NAME}/buildWithParameters" "${PARAMS[@]}"
    exit 0



elif [[ "$ACTION" == "copy" || "$ACTION" == "copy-keep" ]]; then
    #tmpVar=JENKINS_${SRC_URL##*:}
    CONFIGXML=$(mktemp -t tmp.Jenkinsjobxml.XXXXXX)
    if [[ "$KEEP_XML" == "true" ]]; then
        echo "Job config stored as: $CONFIGXML"
    else
        trap '[[ -z "$CONFIGXML" ]] || rm -f $CONFIGXML' EXIT
    fi

    if [[ -f "$COPY_SRC" ]]; then
        cp "$COPY_SRC" "$CONFIGXML"
    else
        SRC_JENKINS="JENKINS_${COPY_SRC/:*}"
        SRC_CURLOPTS="CURLOPTS_${COPY_SRC/:*}"
        SRC_JOB="${COPY_SRC#*:}"
        # now eval var reference to configuration
        SRC_JENKINS="${!SRC_JENKINS}"
        SRC_CURLOPTS="${CURLOPTS:-} ${!SRC_CURLOPTS}"
        $CURL --fail -o "$CONFIGXML" -X GET -L $SRC_CURLOPTS "${SRC_JENKINS}job/${SRC_JOB}/config.xml"
    fi


    TGT_JENKINS="JENKINS_${COPY_TGT/:*}"
    TGT_CURLOPTS="CURLOPTS_${COPY_TGT/:*}"
    TGT_JOB="${COPY_TGT#*:}"
    # now eval var reference to configuration
    TGT_JENKINS="${!TGT_JENKINS}"
    TGT_CURLOPTS="${CURLOPTS:-} ${!TGT_CURLOPTS}"

    # now try to update if exists
    if $CURL --fail -X POST -L $TGT_CURLOPTS "${TGT_JENKINS}job/${TGT_JOB}/config.xml" --data-binary "@${CONFIGXML}" -H "Content-Type:text/xml"; then
        echo "Job updated"
    else
        echo "Updating failed, attempting to create new job ..."
        # if not then try creating the job
        if $CURL --fail -X POST -L $TGT_CURLOPTS "${TGT_JENKINS}createItem?name=${TGT_JOB}" --data-binary "@${CONFIGXML}" -H "Content-Type:text/xml"; then
            echo "Job created"
        else
            echo "ERROR: Unable to create neither update the job!"
            exit 1
        fi
    fi
    echo "Job url: ${TGT_JENKINS}job/${TGT_JOB}/"
    if [[ "$PREVIEW" == "true" ]]; then
        echo ""
        echo "$PREVIEW_MSG"
    fi
    exit 0
fi


# all following actions are script based
# but only 'script' action has file provided
# rest are built-in (here below) so prepare temp file for them
if [[ "$ACTION" != "script" ]]; then
    SCRIPT="$(mktemp)"
    trap "rm -f $SCRIPT" EXIT
fi


if [[ "$ACTION" = "enable" || "$ACTION" = "disable" ]]; then
    DISABLED="false"
    [[ "$ACTION" = "disable" ]] && DISABLED="true"

    cat > $SCRIPT <<EOF
for(item in hudson.model.Hudson.instance.items) {
    if( "${SEARCHFOR}" == "" || item.name.indexOf("${SEARCHFOR}") >= 0 ) {
        println("Disable=${DISABLED} (was " + item.disabled + ") for " + item.name + " was " + item.disabled)
        if( ! $PREVIEW ) {
            item.disabled = ${DISABLED}
            item.save()
        }
    }
}
EOF

elif [[ "$ACTION" = "queue" ]]; then
    cat > $SCRIPT <<EOF
    q = hudson.model.Hudson.instance.queue
    q.items.findAll {
      it.task.name.indexOf("$SEARCHFOR") >= 0
    }.each {
        if( ! $PREVIEW ) {
            q.cancel(it.task);
            print("Canceled " + it.task.name);
        } else {
            print("Would cancel " + it.task.name);
        }
    }
EOF

CLIDIR=${CLIDIR:-"$HOME/.cache/jenkins-tool"}
if [[ ! -d "$CLIDIR" ]]; then
    mkdir -p "$CLIDIR"
fi

CLI="$CLIDIR/$JENKINS-cli.jar"
if [[ ! -x "$CLI" ]]; then
    CLIURL="$URL/jnlpJars/jenkins-cli.jar"
    echo "Fetching cli.jar from $CLIURL into $CLI"
    curl -k -o "$CLI" "$CLIURL"
    chmod +x "$CLI"
fi
if ! head -n2 "$CLI" | grep -q 'META-INF/PK'; then
    echo "$CLI from ${CLIURL:-??} does not look like jar file!" >&2
    exit 1
fi

set -x
java -jar "$CLI" $EXTRAOPTS -s $URL groovy $SCRIPT
